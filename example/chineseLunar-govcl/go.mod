module chineseLunar

go 1.17

require (
	github.com/starainrt/astro v0.0.0-20220105092628-c0bcbbb42a60
	github.com/ying32/govcl v2.2.0+incompatible
	gitlab.com/amrta2022/xa v1.1.8
)

require (
	github.com/ying32/dylib v0.0.0-20220227124818-fdf9ea9fbc96 // indirect
	github.com/ying32/liblclbinres v0.0.0-20220111074620-df670ba4baf4 // indirect
)
